resource "aws_budgets_budget" "cost" {
  name = var.budget_name
  budget_type  = "COST"
  limit_unit   = "USD"
  limit_amount = var.max_budget
  time_unit    = var.lenght_budget
  time_period_start = var.time_start
  time_period_end   = var.time_end

  notification {
  comparison_operator        = "GREATER_THAN"
  threshold_type             = "PERCENTAGE"
  notification_type          = "FORECASTED"
  threshold                  = var.notif_threshold
  subscriber_email_addresses = var.subscriber_email_addresses
  }

}
