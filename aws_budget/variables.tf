variable "subscriber_email_addresses" {
  type        = list(string)
  description = "The list of email addresses of notification subscribers"
  default     = []
}

variable "budget_name" {
  type        = string
  description = "Name of the budget"
  default     = ""
}

variable "max_budget" {
  type        = string
  description = "Max budget"
  default     = ""
}

variable "time_start" {
    type = string
    default = "2017-07-01_00:00"
}

variable "time_end" {
    type = string
    default = "2087-06-15_00:00"
}

variable "lenght_budget" {
    description = "The length of time until a budget resets the actual and forecasted spend. Valid values: MONTHLY, QUARTERLY, ANNUALLY."
    type = string
    default = "MONTHLY"
}

variable "notif_threshold" {
    description = "Threshold when the notification should be sent."
    type = string
    default = "50"  
}
